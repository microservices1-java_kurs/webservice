package com.app.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by T420 on 2017-10-16.
 */
@Controller
public class IndexController {

    @GetMapping("/")
    public ModelAndView index(){
        return new ModelAndView("index");
    }
}
